const path = require('path')
module.exports = {
  static: {
    directory: path.resolve(__dirname, '../store'),
    cleaner: {
      lifetime: 7 * 24 * 60 * 60 * 1000,
      cron: '00 */30 * * * *'
    }
  },
  storage: {
    s3: false,
    directory: path.resolve(__dirname, '../data'),
    path: '/data', // Not used, tus has a weird interface= path is required but directory, above, is used if defined
    bucket: 'nova-uptonova',
    accessKeyId: '(override)',
    secretAccessKey: '(override)',
    region: 'eu-central-1',
    partSize: 8 * 1024 * 1024,
    tmpDirPrefix: 'tus-s3-store'
  },
  notification: {
    activated: false,
    // hook: ''
  }
}
