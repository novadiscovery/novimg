FROM node:14
MAINTAINER Jean Ponchon <jean.ponchon@novadiscovery.com>

WORKDIR /home/node/app
COPY . /home/node/app
RUN chown -R node:node /home/node/app

USER node

RUN npm ci && npm run build
RUN mkdir -p /home/node/app/store
RUN mkdir -p /home/node/app/data

VOLUME /home/node/app/store

ENV PORT "8080"
ENV NODE_ENV "production"

CMD ["npm", "run", "serve"]
