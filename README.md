# Image hosting for Nova

A simple document upload tool for playground purpose.

Feature:

- Upload one or many files
- Trigger a webhook when a file is uploaded 

*This is a playground project. This project is incomplete, poorly documented, untested and unsecure. Do not use in production.*

## Installation / getting started

### For development

```sh
npm i
npm run watch-node
```

### Run the docker image

```sh
docker run -p 8080:8080 registry.gitlab.com/novadiscovery/novimg/server:master
```


## Configuration

You can set the configuration for the running instance by adding `local.json` file in  `/home/node/app/config/local.json` such has:

```json
{
  "storage": {
    "s3": true,
    "bucket": "BUCKET",
    "accessKeyId": "ACCESS_KEY_ID",
    "secretAccessKey": "SECRET_ACCESS_KEY"
  },
  "notification": {
    "activated": false,
    "hook": "HOOK_URL"
  }
}
```

Exemple, with a simple docker run:

```sh
docker run -p 8080:8080 \
  -v $(pwd)/config/local.json.sample:/home/node/app/config/local.json \
  registry.gitlab.com/novadiscovery/novimg/server:master
```

### Configuration options

#### Disk storage

By default, files will be stored on the local folder `store` (`/home/node/app/store` in docker).

```json
  "static": {
    "directory": ABSOLUTE_FILE_PATH,
    "cleaner": {
      "lifetime": LIFETIME_IN_MS,
      "cron": "00 */30 * * * *"
    }
  }
```


#### Bucket storage

To use a s3 bucket to store uploaded file, you will need to set the bucket uri and the access keys and set the `s3` flag to `true`

```json
  "storage": {
    "s3": true, // Enable s3 storage
    "bucket": "BUCKET",
    "accessKeyId": "ACCESS_KEY_ID",
    "secretAccessKey": "SECRET_ACCESS_KEY"
  }
```




#### HTTP Hook

You can activate the hook system using the `notification` config:

```json
  "notification": {
    "activated": true,
    "hook": "HOOK_URL"
  }
```

This will send a json POST request on the hook uri, with a message following this body schema:

```js
{
  "text": `New file received (FILENAME)`,
  "attachments": [
    {
      "title": FILENAME,
      "text": "Notice that this file could be destroyed in ${LIFETIME}",
      "color": "#008abe"
    }
  ]
}
```

