import cronFileCleaner from 'cron-file-cleaner'
import config from 'config'
import pino from 'pino'

const STATIC_DIRECTORY: string = config.get('static.directory')
const LIFETIME: number = config.get('static.cleaner.lifetime')
const CRON: string = config.get('static.cleaner.cron')
const FileCleaner = cronFileCleaner.FileCleaner

const log = pino()

const fileWatcher = new FileCleaner(STATIC_DIRECTORY, LIFETIME,  CRON, {
  start: true,
  blackList: /\.gitme/
})

log.info({
  config: config.get('static.cleaner')
}, 'Start file cleaner')

fileWatcher.on('delete', (file: string) => log.info(file, 'Delete file'))
fileWatcher.on('error', (err: Error) => log.error(err))
