'use strict'

import express from 'express'
// import { Request } from 'express'
import tus from 'tus-node-server'
import config from 'config'
import got from 'got'
import { kebabCase } from 'lodash'
import path from 'path'
import fs from 'fs-extra'
import moment from 'moment'
import ms from 'ms'

// Flag: store on s3 (currently, not production ready for the tus library)
const STORE_ON_S3: boolean = config.get('storage.s3')
const STATIC_DIRECTORY: string = config.get('static.directory')
const LIFETIME: number = config.get('static.cleaner.lifetime')
const STORAGE_DIRECTORY: string = config.get('storage.directory')
const NOTIFY: boolean = config.get('notification.activated')

// Create a tus server
const server = new tus.Server()

// Prepare data store implementation
if (STORE_ON_S3) {
  server.datastore = new tus.S3Store(
    JSON.parse(JSON.stringify(config.get('storage')))
  )
} else {
  server.datastore = new tus.FileStore(
    JSON.parse(JSON.stringify(config.get('storage')))
  )
}

// Watch successfull upload event
server.on('EVENT_UPLOAD_COMPLETE', async (event: any) => {
  const meta = parseMetaData(event.file.upload_metadata || '')
  try {
    if (STORE_ON_S3) {
      await notify(meta.filename || 'noname')
    } else {
      await handleUpload(event.file.id, meta)
    }
  } catch (error) {
    console.error(error)
  }
})

/**
 * Parse tus client meta data (base64 urlencoded)
 * @param  rawMeta string
 */
function parseMetaData(rawMeta: string): LooseObject {
  const pairLit = rawMeta.split(',')

  return pairLit.reduce((metadata: LooseObject, pair: string): LooseObject => {
    const [key, base64value] = pair.split(' ')

    metadata[key] = Buffer.from(base64value || '', 'base64').toString('ascii')

    return metadata
  }, {})
}

/**
 * Handle filestorage upload
 *
 * Rename and move successfull uploaded file to static file storage
 *
 * @param  id   Uniq file identifier
 * @param  meta Tus metadata
 */
async function handleUpload(id: string, meta: LooseObject) {
  const rawFilename = meta.filename || 'noname'
  const extname = path.extname(rawFilename)
  const ext = (kebabCase(extname.slice(1)) || 'bin').slice(0, 10)
  const filename = `${moment()
    .utc()
    .format('YYYY-MM-DDTHHmmss')}Z_${id}_${kebabCase(
    path.basename(rawFilename, extname).slice(0, 200)
  )}.${ext}`
  const filepath = path.resolve(STORAGE_DIRECTORY, id)
  const outpath = path.resolve(STATIC_DIRECTORY, filename)
  console.log({ filepath, outpath })
  try {
    await fs.move(filepath, outpath)
    await notify(filename)
  } catch (error) {
    console.error(error)
  }
}

/**
 * Send a rocket-chat notification for new stored file
 * @param  filename new filename
 * @return
 */
async function notify(filename: string) {
  if (!NOTIFY) {
    return
  }
  const body: LooseObject = {
    text: `New file received (${filename})`,
    attachments: [
      {
        title: filename,
        text: `Notice that this file could be destroyed in ${ms(LIFETIME)}`,
        color: '#008abe'
      }
    ]
  }
  await got.post(config.get('notification.hook'), {
    json: true,
    body
  })
}

// Expose upload router
const router = express.Router()
router.all('*', server.handle.bind(server))

export default router
