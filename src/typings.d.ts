declare module 'tus-node-server'
declare module 'express-pino-logger'
declare module 'cron-file-cleaner' {
  export class FileCleaner {
    constructor(path: string, threshold: number, interval: string, options?: LooseObject)
    on(event: string, callback: Function): any
    maxAge: number
    cronTime: string
  }
}

interface LooseObject {
  [key: string]: any
}
