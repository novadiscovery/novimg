import express from 'express'
// import compression from 'compression'
import bodyParser from 'body-parser'
import lusca from 'lusca'
import path from 'path'
import up from './up'
import config from 'config'
import expressPino from 'express-pino-logger'
import './cleaner'


const STATIC_DIRECTORY: string = config.get('static.directory')

// Create Express server
const app = express()

// Express configuration
app.set('port', process.env.PORT || 8080)
app.set('views', path.join(__dirname, '../views'))
app.set('view engine', 'pug')
app.use(expressPino())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(lusca.xframe('SAMEORIGIN'))
app.use(lusca.xssProtection(true))
// app.use(compression())

app.use(
  express.static(path.join(__dirname, 'public'), { maxAge: 3600000 * 24 })
)

app.use('/store',
  express.static(STATIC_DIRECTORY, { maxAge: 3600000 * 24 })
)

app.use('/upload', up)

export default app
