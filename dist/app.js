"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
// import compression from 'compression'
const body_parser_1 = __importDefault(require("body-parser"));
const lusca_1 = __importDefault(require("lusca"));
const path_1 = __importDefault(require("path"));
const up_1 = __importDefault(require("./up"));
const config_1 = __importDefault(require("config"));
const express_pino_logger_1 = __importDefault(require("express-pino-logger"));
require("./cleaner");
const STATIC_DIRECTORY = config_1.default.get('static.directory');
// Create Express server
const app = express_1.default();
// Express configuration
app.set('port', process.env.PORT || 8080);
app.set('views', path_1.default.join(__dirname, '../views'));
app.set('view engine', 'pug');
app.use(express_pino_logger_1.default());
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(lusca_1.default.xframe('SAMEORIGIN'));
app.use(lusca_1.default.xssProtection(true));
// app.use(compression())
app.use(express_1.default.static(path_1.default.join(__dirname, 'public'), { maxAge: 3600000 * 24 }));
app.use('/store', express_1.default.static(STATIC_DIRECTORY, { maxAge: 3600000 * 24 }));
app.use('/upload', up_1.default);
exports.default = app;
//# sourceMappingURL=app.js.map