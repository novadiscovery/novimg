"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cron_file_cleaner_1 = __importDefault(require("cron-file-cleaner"));
const config_1 = __importDefault(require("config"));
const pino_1 = __importDefault(require("pino"));
const STATIC_DIRECTORY = config_1.default.get('static.directory');
const LIFETIME = config_1.default.get('static.cleaner.lifetime');
const CRON = config_1.default.get('static.cleaner.cron');
const FileCleaner = cron_file_cleaner_1.default.FileCleaner;
const log = pino_1.default();
const fileWatcher = new FileCleaner(STATIC_DIRECTORY, LIFETIME, CRON, {
    start: true,
    blackList: /\.gitme/
});
log.info({
    config: config_1.default.get('static.cleaner')
}, 'Start file cleaner');
fileWatcher.on('delete', (file) => log.info(file, 'Delete file'));
fileWatcher.on('error', (err) => log.error(err));
//# sourceMappingURL=cleaner.js.map