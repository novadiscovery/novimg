'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
// import { Request } from 'express'
const tus_node_server_1 = __importDefault(require("tus-node-server"));
const config_1 = __importDefault(require("config"));
const got_1 = __importDefault(require("got"));
const lodash_1 = require("lodash");
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const moment_1 = __importDefault(require("moment"));
const ms_1 = __importDefault(require("ms"));
// Flag: store on s3 (currently, not production ready for the tus library)
const STORE_ON_S3 = config_1.default.get('storage.s3');
const STATIC_DIRECTORY = config_1.default.get('static.directory');
const LIFETIME = config_1.default.get('static.cleaner.lifetime');
const STORAGE_DIRECTORY = config_1.default.get('storage.directory');
const NOTIFY = config_1.default.get('notification.activated');
// Create a tus server
const server = new tus_node_server_1.default.Server();
// Prepare data store implementation
if (STORE_ON_S3) {
    server.datastore = new tus_node_server_1.default.S3Store(JSON.parse(JSON.stringify(config_1.default.get('storage'))));
}
else {
    server.datastore = new tus_node_server_1.default.FileStore(JSON.parse(JSON.stringify(config_1.default.get('storage'))));
}
// Watch successfull upload event
server.on('EVENT_UPLOAD_COMPLETE', (event) => __awaiter(this, void 0, void 0, function* () {
    const meta = parseMetaData(event.file.upload_metadata || '');
    try {
        if (STORE_ON_S3) {
            yield notify(meta.filename || 'noname');
        }
        else {
            yield handleUpload(event.file.id, meta);
        }
    }
    catch (error) {
        console.error(error);
    }
}));
/**
 * Parse tus client meta data (base64 urlencoded)
 * @param  rawMeta string
 */
function parseMetaData(rawMeta) {
    const pairLit = rawMeta.split(',');
    return pairLit.reduce((metadata, pair) => {
        const [key, base64value] = pair.split(' ');
        metadata[key] = Buffer.from(base64value || '', 'base64').toString('ascii');
        return metadata;
    }, {});
}
/**
 * Handle filestorage upload
 *
 * Rename and move successfull uploaded file to static file storage
 *
 * @param  id   Uniq file identifier
 * @param  meta Tus metadata
 */
function handleUpload(id, meta) {
    return __awaiter(this, void 0, void 0, function* () {
        const rawFilename = meta.filename || 'noname';
        const extname = path_1.default.extname(rawFilename);
        const ext = (lodash_1.kebabCase(extname.slice(1)) || 'bin').slice(0, 10);
        const filename = `${moment_1.default()
            .utc()
            .format('YYYY-MM-DDTHHmmss')}Z_${id}_${lodash_1.kebabCase(path_1.default.basename(rawFilename, extname).slice(0, 200))}.${ext}`;
        const filepath = path_1.default.resolve(STORAGE_DIRECTORY, id);
        const outpath = path_1.default.resolve(STATIC_DIRECTORY, filename);
        console.log({ filepath, outpath });
        try {
            yield fs_extra_1.default.move(filepath, outpath);
            yield notify(filename);
        }
        catch (error) {
            console.error(error);
        }
    });
}
/**
 * Send a rocket-chat notification for new stored file
 * @param  filename new filename
 * @return
 */
function notify(filename) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!NOTIFY) {
            return;
        }
        const body = {
            text: `New file received (${filename})`,
            attachments: [
                {
                    title: filename,
                    text: `Notice that this file could be destroyed in ${ms_1.default(LIFETIME)}`,
                    color: '#008abe'
                }
            ]
        };
        yield got_1.default.post(config_1.default.get('notification.hook'), {
            json: true,
            body
        });
    });
}
// Expose upload router
const router = express_1.default.Router();
router.all('*', server.handle.bind(server));
exports.default = router;
//# sourceMappingURL=up.js.map