/* global tus */
/* eslint no-console: 0 */

'use strict'

var upload = null
var uploadIsRunning = false
var toggleBtn = document.querySelector('#toggle-btn')
var resumeCheckbox = document.querySelector('#resume')
var input = document.querySelector('input[type=file]')
var progress = document.querySelector('.progress')
var progressBar = progress.querySelector('#progress-bar')
var alertBox = document.querySelector('#support-alert')
var uploadList = document.querySelector('#upload-list')
var chunkInput = document.querySelector('#chunksize')
var endpointInput = document.querySelector('#endpoint')

if (!tus.isSupported) {
  alertBox.classList.remove('d-none')
}

if (!toggleBtn) {
  throw new Error('Toggle button not found on this page. Aborting upload-demo. ')
}

toggleBtn.addEventListener('click', function (e) {
  e.preventDefault()

  if (upload) {
    if (uploadIsRunning) {
      upload.abort()
      toggleBtn.textContent = 'resume upload'
      uploadIsRunning = false
    } else {
      upload.start()
      toggleBtn.textContent = 'pause upload'
      uploadIsRunning = true
    }
  } else {
    if (input.files.length > 0) {
      startUpload()
    } else {
      input.click()
    }
  }
})

input.addEventListener('change', startUpload)

function startUpload () {
  var file = input.files[0]
  // Only continue if a file has actually been selected.
  // IE will trigger a change event even if we reset the input element
  // using reset() and we do not want to blow up later.
  if (!file) {
    return
  }

  var endpoint = '/upload'
  var chunkSize = 10 * 1024 * 1024 * 1024
  if (isNaN(chunkSize)) {
    chunkSize = Infinity
  }

  toggleBtn.textContent = 'pause upload'

  var options = {
    endpoint: endpoint,
    resume: !resumeCheckbox.checked,
    chunkSize: chunkSize,
    retryDelays: [0, 1000, 3000, 5000],
    metadata: {
      filename: file.name,
      filetype: file.type
    },
    onError: function (error) {
      if (error.originalRequest) {
        if (window.confirm('Failed.\nDo you want to retry?')) {
          upload.start()
          uploadIsRunning = true
          return
        } else {
          addResultRow(upload.file.name, true)
        }
      } else {
        window.alert('Failed because: ' + error)
        addResultRow(upload.file.name, true)
      }

      reset()
    },
    onProgress: function (bytesUploaded, bytesTotal) {
      var percentage = (bytesUploaded / bytesTotal * 100).toFixed(2)
      progressBar.style.width = percentage + '%'
      console.log(bytesUploaded, bytesTotal, percentage + '%')

      progressBar.classList.add('progress-bar-striped')
    },
    onSuccess: function () {
      addResultRow(upload.file.name + ' (' + upload.file.size + ' bytes)')
      progressBar.classList.remove('progress-bar-striped')
      reset()
    }
  }

  upload = new tus.Upload(file, options)
  upload.start()
  uploadIsRunning = true
}

function addResultRow (message, error) {
  var li = document.createElement('li')
  li.className = 'list-group-item'

  var badge = document.createElement('span')
  if (!error) {
    badge.textContent = 'success'
    badge.className = 'badge badge-success mr-3'
    li.appendChild(badge)
  } else {
    badge.textContent = 'error'
    badge.className = 'badge badge-danger mr-3'
    li.appendChild(badge)
  }

  var msg = document.createElement('span')
  msg.textContent = message
  li.appendChild(msg)
  uploadList.appendChild(li)
}

function reset () {
  input.value = ''
  toggleBtn.textContent = 'start upload'
  upload = null
  uploadIsRunning = false
  setTimeout(function () {
    if (!uploadIsRunning) {
      progressBar.style.width = '0%'
    }
  }, 2000)
}
